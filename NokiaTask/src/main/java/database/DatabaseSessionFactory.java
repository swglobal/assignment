package database;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseSessionFactory {

  public static SessionFactory getDatabaseSession(Class clz) {
    try {
      // create a new connection
      return new Configuration().configure().addAnnotatedClass(clz)
          .buildSessionFactory();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
