import java.util.logging.Level;
import utils.MenuOptions;
import models.menu.MenuCategory;

public class Main {

  public static void main(String[] args) {
    startApp();
  }

  private static void startApp() {
    setHibernateLogLevel();

    //Shared object to load menus for every option
    try {
      //loading initial menu from memory
      MenuOptions.shared.loadMenus("/menus.json");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    //displaying initial menu
    MenuOptions.shared.showMenus(MenuCategory.INITIAL);
  }

  private static void setHibernateLogLevel() {
    java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
  }
}
