package handlers.menuHandlers;

import database.repository.ManufacturerRepository;
import database.repository.PartManufacturerRepository;
import entities.Manufacturer;
import entities.PartManufacturer;
import interfaces.IManufacturerMenuHandler;
import interfaces.IMenuHandler;
import java.util.Optional;
import models.menu.Menu;
import services.ManufacturerService;

import java.lang.reflect.InvocationTargetException;
import services.PartService;
import usecases.ManufacturerUseCase;

public class ManufacturerMenuHandler implements IMenuHandler, IManufacturerMenuHandler {

  ManufacturerUseCase useCase = new ManufacturerUseCase();

  @Override
  public void handleMenu(Menu menu) {
    try {
      this.getClass().getDeclaredMethod(menu.getMethod(), Menu.class).invoke(this, menu);
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void addQuantity(Menu menu) {
    useCase.startAddQuantityFlow();
    goBack(menu);
  }

  @Override
  public void listQuantity(Menu menu) {

    useCase.startListQuantityFlow();
    goBack(menu);
  }

  @Override
  public void goBack(Menu menu) {
    IMenuHandler.menuOptions.goBack(menu);
  }
}
