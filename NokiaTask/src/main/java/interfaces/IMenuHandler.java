package interfaces;

import models.menu.Menu;
import utils.MenuOptions;

public interface IMenuHandler {

    MenuOptions menuOptions = MenuOptions.shared;

    void handleMenu(Menu menu);

    void goBack(Menu menu);
}
